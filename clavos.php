<?php
/**
 * Plugin Name:     Clavos
 * Description:     Starter Plugin for Custom Functionality
 * Author:          Page Design Group
 * Text Domain:     clavos
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         clavos
 */

if (!defined('WPINC')) {
    die;
}

include __DIR__ . '/includes/cpts.php';

/**
 * This is an example of how to create a post type using the CPT factory
 */

// $news = new Custom_Post_Type('News');
// $news->add_taxonomy('News Type');
// $meetings = new Custom_Post_Type('Meetings');
